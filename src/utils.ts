import cheerio from 'cheerio';
import axios from 'axios';
import emojiFlags from 'emoji-flags';
import { stringify } from 'qs';

async function fetchData(bin: string): Promise<string> {
    let resp = await axios({
        url: 'http://bins.su/',
        method: 'POST',
        headers: {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'en-IN,en-GB;q=0.9,en-US;q=0.8,en;q=0.7,bn;q=0.6',
            'Connection': 'keep-alive',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Host': 'bins.su',
            'Origin': 'http://bins.su',
            'Referer': 'http://bins.su/',
            'Upgrade-Insecure-Requests': '1',
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.106 Mobile Safari/537.36',
            'Cache-Control': 'max-age=0',
            'Content-Length': '44'
        },
        data: stringify({
            'action': 'searchbins',
            'bins': bin,
            'bank': '',
            'country': ''
        })
    })
    return resp.data;
}

function capitalize(str: string): string {
    let text = str.split(" ");
    return text.map(value => value.charAt(0).toUpperCase() + value.slice(1).toLocaleLowerCase()).join(" ");
}

export async function search(bin: string): Promise<string> {
    bin = bin.substr(0, 6);
    let isNumber = /^\d+$/.test(bin);
    if (bin.length < 6 || !isNumber) {
        return "❗️ <b>Request a valid BIN.</b>";
    }
    let htmlData = await fetchData(bin);
    let $ = cheerio.load(htmlData);
    let result = $("#result").html();
    if (!result || !result.match("Total found 1 bins")) {
        return "❌ <b>No Result Found.</b>";
    }

    let country = $("#result tr:nth-child(2) td:nth-child(2)").text();
    let vendor = $("#result tr:nth-child(2) td:nth-child(3)").text();
    let type = $("#result tr:nth-child(2) td:nth-child(4)").text();
    let level = $("#result tr:nth-child(2) td:nth-child(5)").text();
    let bank = $("#result tr:nth-child(2) td:nth-child(6)").text();

    let resp = `➥ <b>BIN :</b> <code>${bin}</code>\n` +
        `➥ <b>Brand :</b> <i>${capitalize(vendor)}</i>\n` +
        `➥ <b>Type :</b> <i>${capitalize(type)}</i>\n` +
        `➥ <b>Level :</b> <i>${capitalize(level)}</i>\n` +
        `➥ <b>Bank :</b> <i>${capitalize(bank)}</i>\n` +
        `➥ <b>Country :</b> ${emojiFlags.countryCode(country).name} (${emojiFlags.countryCode(country).emoji})`;
    return resp;
}


/**
 * Thanks to https://gist.github.com/julia-mareike/200e34dacef83d1933d57d1caac9ea86
 */

const isEven = (number: number) => number % 2 === 0
const calculateEven = (even: number) => (even * 2 < 10) ? even * 2 : even * 2 - 9

const generateCheckSum = (card: string) => {
    const checksum = card.split('')
        .map((number, index) => (isEven(index)) ? calculateEven(Number(number)) : parseInt(number, 10))
        .reduce((previous, current) => previous + current) % 10
    return checksum === 0 ? 0 : 10 - checksum
}

export function genCC(bin: string): string {
    let random = Math.random().toString().slice(2, 11);
    if (bin.length === 6) return bin + random;
    let card: string = bin.slice(0, 6);
    for (let i = 0; i < 9; i++) {
        card += parseInt(bin[6 + i]) ? bin[6 + i] : random[i];
    }
    const checksum = generateCheckSum(card);
    return card + checksum;
}

export function randomIntFromInterval(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}




export function getMonth() {
    return randomIntFromInterval(1, 12).toString().padStart(2, "0");
}

export function getYear() {
    let date = new Date();
    return date.getFullYear() + randomIntFromInterval(1, 5);
}

export function getCvv() {
    return randomIntFromInterval(0, 999).toString().padStart(3,"0");
}