import { Composer } from 'telegraf';

export const HelpCommand = Composer.command('help', async ctx => {
    await ctx.replyWithHTML(
        "<b>Commands : </b>\n" +
        "<b>───────────────────────</b>\n" +
        "/help - get this help menu\n" +
        "/about - get info about me\n" +
        "/bin - Search bin details,\n<code>e.g: /bin 111111</code>\n" +
        "/cc or /gen - Generate Cards from bin"
    )
})