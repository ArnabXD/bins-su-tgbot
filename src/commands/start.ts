import { Composer } from 'telegraf';

export const StartCommand = Composer.command('start', async (ctx) => {
    await ctx.replyWithMarkdownV2(
        `*Hi [${ctx.from.first_name}](tg://user?id=${ctx.from.id})*\n` +
        `*──────────────────*\n` +
        `_Check my Commands List :_  \n` +
        `» /help – get commands list \n` +
        `» /about – get info about me`
    );
});