import { StartCommand } from './start';
import { BinCommand } from './bin';
import { AboutCommand } from './about';
import { HelpCommand } from './help';
import { CCgen } from './ccgen';
import bot from '../bot';

const Commands = (): void => {
    bot.use(StartCommand);
    bot.use(BinCommand);
    bot.use(AboutCommand);
    bot.use(HelpCommand);
    bot.use(CCgen);
}

export default Commands;