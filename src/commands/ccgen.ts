import { Composer } from 'telegraf';
import { genCC, getMonth, getYear, getCvv } from '../utils';

const helpStr: string = `<b>Command Usage :</b>\n` +
    `➥ <code>/gen 123456</code>\n` +
    `➥ <code>/gen 123456xxxx45x 10</code>`


export const CCgen = Composer.command(['cc', 'gen'], async ctx => {
    let { text } = ctx.message;
    let [_, bin, number] = text.split(" ");
    if (!bin) return await ctx.replyWithHTML(helpStr);
    let amount = Number(number);
    let card: string = `<b>Bin :</b> <code>${bin}</code>\n<b>───────────────────────</b>\n`;
    if (amount) {
        if (amount > 100) return await ctx.reply("Maximum Limit is 100");
        for (let i = 0; i < amount; i++) {
            card += `<code>${genCC(bin)}|${getMonth()}|${getYear()}|${getCvv()}</code>\n`
        }
        return await ctx.replyWithHTML(card);
    }
    card += `<code>${genCC(bin)}|${getMonth()}|${getYear()}|${getCvv()}</code>`;
    return await ctx.replyWithHTML(card);
})