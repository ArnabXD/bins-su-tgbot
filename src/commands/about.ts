import { Composer } from 'telegraf';

export const AboutCommand = Composer.command('about', async ctx => {
    await ctx.replyWithHTML(
        "<b>Hi There, I am " + ctx.botInfo.first_name + "</b>\n" +
        "<b>────────────────────────────</b>\n" +
        "➥ I Can Fetch BIN details for you from <a href=\"http://bins.su\">Bins.Su</a>\n" +
        "➥ My Source Code :  <a href='https://gitlab.com/ArnabXD/bins-su-tgbot'>Bins-Su-TGbot</a>\n" +
        "➥ For API you can check <a href='https://github.com/ArnabXD/bins-su-api'>this</a>\n" +
        "➥ Bugs/Error/Suggestions : Contact this <a href='tg://user?id=611816596'>noob</a>", {
        disable_web_page_preview: true
    }
    )
})