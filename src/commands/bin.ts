import { Composer } from 'telegraf';
import { search } from '../utils';

export const BinCommand = Composer.command('bin', async ctx => {
    let bin = ctx.message.text.split(" ", 2)[1];
    if (!bin || bin === "") return await ctx.replyWithMarkdown("*Command Usage :* `/bin 111111`");
    let data = await search(bin);
    await ctx.replyWithHTML(data);
});