import { Telegraf } from 'telegraf';

const env = process.env;

const bot = new Telegraf(env.BOT_TOKEN!, { telegram: { webhookReply: false } });
export default bot;