require("dotenv").config();
import bot from './bot';
import useCommands from './commands';

(async () => {
    useCommands();

    if (process.env.DOMAIN && Number(process.env.PORT)) {
        await bot.launch({ webhook: { domain: process.env.DOMAIN, port: Number(process.env.PORT) } });
        console.log(`[BOT] - Running... (${process.env.DOMAIN}:${process.env.PORT})`);
    } else {
        await bot.launch();
        console.log(`[BOT] - Running...`);
    }
})();