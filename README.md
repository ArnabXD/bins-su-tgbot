# XDBinsBot

A [Telegram Bot](https://telegram.dog/xdbinsbot) that can fetch bin details for you from Bins.su

For Bins.Su API checkout [bins-su-api](https://github.com/ArnabXd/bins-su-api)

## ENV Vars
```ini
BOT_TOKEN=YOUR_BOT_TOKEN_FROM_@BOTFATHER
#IF YOU USE WEBHOOK --------------------
DOMAIN=YOUR_BOT_DOMAIN
PORT=PORT_NUBER 
# PORT isn't required for Heroku Users
```